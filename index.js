const txt_first_name = document.querySelector('#txt-first-name')
const txt_last_name = document.querySelector('#txt-last-name')
const span_full_name = document.querySelector('#span-full-name')

// Solution using arrow function
/*txt_first_name.addEventListener('keyup', () => {
	span_full_name.innerHTML = txt_first_name.value
})

txt_last_name.addEventListener('keyup', () => {
	span_full_name.innerHTML = txt_first_name.value + ' ' + txt_last_name.value
})*/

// Solution using a separate function, I'm not sure if eto po yung ibig sabihin sa activity step 3-a
txt_first_name.addEventListener('keyup', showFullName)
txt_last_name.addEventListener('keyup', showFullName)

function showFullName() {
  span_full_name.innerHTML = txt_first_name.value + ' ' + txt_last_name.value
}